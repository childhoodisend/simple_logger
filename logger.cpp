#include <iostream>
#include <ctime>

#include "logger.h"

using namespace Logging;

logger *logger::m_Instance = nullptr;

const std::string logFileName = "logfile.log";


logger::logger(const std::string &str) {
    file_name = str;
    m_File.open(file_name.c_str(), std::ios::out | std::ofstream::app);
    m_LogLevel = LOG_LEVEL_TRACE;
    m_LogType = FILE_LOG;
}

logger::~logger() {
    m_File.close();
}

logger *logger::getInstance(const std::string &str) noexcept {
    if (m_Instance == nullptr) {
        m_Instance = new logger(str);
    }

    return m_Instance;
}

void logger::logIntoFile(std::string &data) {
    std::lock_guard<std::mutex> lock(m);
    m_File << getCurrentTime() << "  " << data << std::endl;
}

void logger::logOnConsole(std::string &data) {
    std::cout << getCurrentTime() << "  " << data << std::endl;
}

std::string logger::getCurrentTime() {
    std::string currTime;
    //Current date/time based on current time
    time_t now = time(nullptr);
    // Convert current time to string
    currTime.assign(ctime(&now));

    // Last charactor of currentTime is "\n", so remove it
    std::string currentTime = currTime.substr(0, currTime.size() - 1);
    return currentTime;
}

// Interface for Error Log
void logger::error(const char *text) noexcept {
    std::string data;
    data.append("[ERROR]: ");
    data.append(text);

    // ERROR must be capture
    if (m_LogType == FILE_LOG) {
        logIntoFile(data);
    } else if (m_LogType == CONSOLE) {
        logOnConsole(data);
    }
}

void logger::error(const std::string &text) noexcept {
    error(text.data());
}

void logger::error(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    error(text.data());
}

// Interface for Alarm Log 
void logger::alarm(const char *text) noexcept {
    std::string data;
    data.append("[ALARM]: ");
    data.append(text);

    // ALARM must be capture
    if (m_LogType == FILE_LOG) {
        logIntoFile(data);
    } else if (m_LogType == CONSOLE) {
        logOnConsole(data);
    }
}

void logger::alarm(const std::string &text) noexcept {
    alarm(text.data());
}

void logger::alarm(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    alarm(text.data());
}

// Interface for Always Log 
void logger::always(const char *text) noexcept {
    std::string data;
    data.append("[ALWAYS]: ");
    data.append(text);

    // No check for ALWAYS logs
    if (m_LogType == FILE_LOG) {
        logIntoFile(data);
    } else if (m_LogType == CONSOLE) {
        logOnConsole(data);
    }
}

void logger::always(const std::string &text) noexcept {
    always(text.data());
}

void logger::always(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    always(text.data());
}

// Interface for Buffer Log 
void logger::buffer(const char *text) noexcept {
    // Buffer is the special case. So don't add log level
    // and timestamp in the buffer message. Just log the raw bytes.
    if ((m_LogType == FILE_LOG) && (m_LogLevel >= LOG_LEVEL_BUFFER)) {
        std::lock_guard<std::mutex> lock(m);
        m_File << text << std::endl;
    } else if ((m_LogType == CONSOLE) && (m_LogLevel >= LOG_LEVEL_BUFFER)) {
        std::cout << text << std::endl;
    }
}

void logger::buffer(const std::string &text) noexcept {
    buffer(text.data());
}

void logger::buffer(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    buffer(text.data());
}

// Interface for Info Log
void logger::info(const char *text) noexcept {
    std::string data;
    data.append("[INFO]: ");
    data.append(text);

    if ((m_LogType == FILE_LOG) && (m_LogLevel >= LOG_LEVEL_INFO)) {
        logIntoFile(data);
    } else if ((m_LogType == CONSOLE) && (m_LogLevel >= LOG_LEVEL_INFO)) {
        logOnConsole(data);
    }
}

void logger::info(const std::string &text) noexcept {
    info(text.data());
}

void logger::info(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    info(text.data());
}

// Interface for Trace Log
void logger::trace(const char *text) noexcept {
    std::string data;
    data.append("[TRACE]: ");
    data.append(text);

    if ((m_LogType == FILE_LOG) && (m_LogLevel >= LOG_LEVEL_TRACE)) {
        logIntoFile(data);
    } else if ((m_LogType == CONSOLE) && (m_LogLevel >= LOG_LEVEL_TRACE)) {
        logOnConsole(data);
    }
}

void logger::trace(const std::string &text) noexcept {
    trace(text.data());
}

void logger::trace(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    trace(text.data());
}

// Interface for Debug Log
void logger::debug(const char *text) noexcept {
    std::string data;
    data.append("[DEBUG]: ");
    data.append(text);

    if ((m_LogType == FILE_LOG) && (m_LogLevel >= LOG_LEVEL_DEBUG)) {
        logIntoFile(data);
    } else if ((m_LogType == CONSOLE) && (m_LogLevel >= LOG_LEVEL_DEBUG)) {
        logOnConsole(data);
    }
}

void logger::debug(const std::string &text) noexcept {
    debug(text.data());
}

void logger::debug(std::ostringstream &stream) noexcept {
    std::string text = stream.str();
    debug(text.data());
}

// Interfaces to control log levels
void logger::updateLogLevel(LogLevel logLevel) {
    m_LogLevel = logLevel;
}

// Enable all log levels
void logger::enableLog() {
    m_LogLevel = ENABLE_LOG;
}

// Disable all log levels, except error and alarm
void logger::disableLog() {
    m_LogLevel = DISABLE_LOG;
}

// Interfaces to control log Types
void logger::updateLogType(LogType logType) {
    m_LogType = logType;
}

void logger::enableConsoleLogging() {
    m_LogType = CONSOLE;
}

void logger::enableFileLogging() {
    m_LogType = FILE_LOG;
}

