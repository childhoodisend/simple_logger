#include <iostream>
#include "logger.h"

using namespace Logging;

int main() {

    LOG_ERROR("Message Logged using Direct Interface, Log level: LOG_ERROR");
    LOG_ALARM("Message Logged using Direct Interface, Log level: LOG_ALARM");
    LOG_ALWAYS("Message Logged using Direct Interface, Log level: LOG_ALWAYS");
    LOG_INFO("Message Logged using Direct Interface, Log level: LOG_INFO");
    LOG_BUFFER("Message Logged using Direct Interface, Log level: LOG_BUFFER");
    LOG_TRACE("Message Logged using Direct Interface, Log level: LOG_TRACE");
    LOG_DEBUG("Message Logged using Direct Interface, Log level: LOG_DEBUG");


    logger *pLogger = nullptr; // Create the object pointer for logger Class
    pLogger = logger::getInstance();

    pLogger->error("Message Logged using C++ Interface, Log level: LOG_ERROR");
    pLogger->alarm("Message Logged using C++ Interface, Log level: LOG_ALARM");
    pLogger->always("Message Logged using C++ Interface, Log level: LOG_ALWAYS");
    pLogger->buffer("Message Logged using C++ Interface, Log level: LOG_INFO");
    pLogger->info("Message Logged using C++ Interface, Log level: LOG_BUFFER");
    pLogger->trace("Message Logged using C++ Interface, Log level: LOG_TRACE");
    pLogger->debug("Message Logged using C++ Interface, Log level: LOG_DEBUG");


    std::string str = "some string";
    std::ostringstream ss;
    ss << str << std::endl;

    pLogger->enableConsoleLogging();
    pLogger->updateLogLevel(LOG_LEVEL_INFO);

    LOG_ALWAYS("Logging ostringstream using Direct Interface");
    LOG_ERROR(ss);
    LOG_ALARM(ss);
    LOG_ALWAYS(ss);
    LOG_INFO(ss);
    LOG_BUFFER(ss);
    LOG_TRACE(ss);
    LOG_DEBUG(ss);

    logger::getInstance()->buffer("Logging ostringstream using C++ Interface");
    logger::getInstance()->error("str");
    logger::getInstance()->alarm(ss);
    logger::getInstance()->always(ss);
    logger::getInstance()->buffer(ss);
    logger::getInstance()->info(ss);
    logger::getInstance()->trace(ss);
    logger::getInstance()->debug("str");

    return EXIT_SUCCESS;
}