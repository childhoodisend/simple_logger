#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <iostream>
#include <memory>
#include <fstream>
#include <sstream>
#include <string>
#include <mutex>

#include <cerrno>
#include <pthread.h>

namespace Logging {

#define LOG_ERROR(x)    logger::getInstance()->error(x)
#define LOG_ALARM(x)    logger::getInstance()->alarm(x)
#define LOG_ALWAYS(x)   logger::getInstance()->always(x)
#define LOG_INFO(x)     logger::getInstance()->info(x)
#define LOG_BUFFER(x)   logger::getInstance()->buffer(x)
#define LOG_TRACE(x)    logger::getInstance()->trace(x)
#define LOG_DEBUG(x)    logger::getInstance()->debug(x)

    typedef enum LOG_LEVEL {
        DISABLE_LOG = 1,
        LOG_LEVEL_INFO = 2,
        LOG_LEVEL_BUFFER = 3,
        LOG_LEVEL_TRACE = 4,
        LOG_LEVEL_DEBUG = 5,
        ENABLE_LOG = 6,
    } LogLevel;

    typedef enum LOG_TYPE {
        NO_LOG = 1,
        CONSOLE = 2,
        FILE_LOG = 3,
    } LogType;

    class logger {
    public:
        logger() = delete;

        explicit logger(const std::string &file_name = "logfile.log");

        ~logger();

        static logger *getInstance(const std::string &str = "logfile.log") noexcept;

        void error(const char *text) noexcept;

        void error(const std::string &text) noexcept;

        void error(std::ostringstream &stream) noexcept;

        template<typename T>
        void error(T num) noexcept {
            std::string text = std::to_string(num);
            error(text.data());
        }

        void alarm(const char *text) noexcept;

        void alarm(const std::string &text) noexcept;

        void alarm(std::ostringstream &stream) noexcept;

        template<typename T>
        void alarm(T num) noexcept {
            std::string text = std::to_string(num);
            alarm(text.data());
        }

        void always(const char *text) noexcept;

        void always(const std::string &text) noexcept;

        void always(std::ostringstream &stream) noexcept;

        template<typename T>
        void always(T num) noexcept {
            std::string text = std::to_string(num);
            always(text.data());
        }

        void buffer(const char *text) noexcept;

        void buffer(const std::string &text) noexcept;

        void buffer(std::ostringstream &stream) noexcept;

        template<typename T>
        void buffer(T num) noexcept {
            std::string text = std::to_string(num);
            buffer(text.data());
        }

        void info(const char *text) noexcept;

        void info(const std::string &text) noexcept;

        void info(std::ostringstream &stream) noexcept;

        template<typename T>
        void info(T num) noexcept {
            std::string text = std::to_string(num);
            info(text.data());
        }

        void trace(const char *text) noexcept;

        void trace(const std::string &text) noexcept;

        void trace(std::ostringstream &stream) noexcept;

        template<typename T>
        void trace(T num) noexcept {
            std::string text = std::to_string(num);
            trace(text.data());
        }


        void debug(const char *text) noexcept;

        void debug(const std::string &text) noexcept;

        void debug(std::ostringstream &stream) noexcept;

        template<typename T>
        void debug(T num) noexcept {
            std::string text = std::to_string(num);
            debug(text.data());
        }

        // Error and Alarm log must be always enable
        // Hence, there is no interfce to control error and alarm logs

        // Interfaces to control log levels
        void updateLogLevel(LogLevel logLevel);

        void enableLog();  // Enable all log levels
        void disableLog(); // Disable all log levels, except error and alarm

        // Interfaces to control log Types
        void updateLogType(LogType logType);

        void enableConsoleLogging();

        void enableFileLogging();

    protected:

        std::string getCurrentTime();

    private:
        void logIntoFile(std::string &data);

        void logOnConsole(std::string &data);

    private:
        static logger *m_Instance;
        std::ofstream m_File;

        LogLevel m_LogLevel;
        LogType m_LogType;

        std::mutex m;
        std::string file_name;
    };

    using logger_ptr = std::shared_ptr<logger>;
}
#endif //_LOGGER_H_
