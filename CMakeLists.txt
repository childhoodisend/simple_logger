cmake_minimum_required(VERSION 3.1)
project(logger)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wfloat-conversion -pedantic -pthread")

add_library(logger SHARED logger.h logger.cpp)
